<?php
/**
 * @file
 * Views hooks for converge_session feature
 */

/**
 * Implementats hook_views_plugins().
 */
function converge_session_views_plugins() {
  return array(
    'module' => 'converge_session',
    'style' => array(
      'converge_session_schedule' => array(
        'title' => t('Session schedule grid'),
        'help' => t('Display view as a table with room columns and time slot rows'),
        'handler' => 'converge_session_views_plugin_style_schedule',
        'theme' => 'converge_session_schedule',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'use sort' => FALSE,
        'use pager' => FALSE,
        'use ajax' => FALSE,
        'use more' => FALSE,
        'type' => 'normal',
        'help topic' => 'style-unformatted',
        'even empty' => TRUE, // Make the View render when there are no results.
        ),
      ),
    );
}
